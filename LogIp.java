import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.iteracja.database.DatabaseNotConnectedException;
import com.iteracja.database.DriverException;


public class LogIp 
{

        public static String getDeviceIp()
        {
            String deviceIp = "";
            try 
            {
                    deviceIp = InetAddress.getLocalHost().toString();
            } 
            catch (UnknownHostException e) 
            {

                    e.printStackTrace();
            }
            
            return deviceIp;
        }
        
        static int action;
        
        public static int getAction() 
        {
                return action;
        }

        public void setAction(int action)
        {
                LogIp.action = action;
        }

        public static void logExecution()
        {

            String stm = "INSERT INTO data.p2s_restarter_log(date,ip,action) VALUES(NOW(),'"+getDeviceIp()+"','"+getAction()+"')";
            try
			{
				Connector.getInstance().executeQuery(stm);
			} 
            catch (DatabaseNotConnectedException | DriverException e)
			{
            	Logger lgr = Logger.getLogger(PreparedStatement.class.getName());
                lgr.log(Level.SEVERE, e.getMessage(), e);
				e.printStackTrace();
			}

      }
}