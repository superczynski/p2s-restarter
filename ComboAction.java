import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;


public class ComboAction implements ActionListener
{

        private static String ip;
        private static String login;
        private static String pass;
        
        
        public static String getIp() 
        {
                return ip;
        }


        public void setIp(String ip) 
        {
                ComboAction.ip = ip;
        }


        public static String getLogin() 
        {
                return login;
        }


        public void setLogin(String login) 
        {
                ComboAction.login = login;
        }


        public static String getPass() 
        {
                return pass;
        }


        public void setPass(String pass) 
        {
                ComboAction.pass = pass;
        }


        @Override
        public void actionPerformed(ActionEvent e)
        {
	        Object source = e.getSource();
	        JComboBox combo = (JComboBox)source;
	        String db = combo.getSelectedItem().toString();
	        if(db.equals("Add-Connect"))
	        {
	                setIp("192.168.24.10");
	                setLogin("ss0");
	                setPass("QWE123?!!");
	        }
	        else if(db.equals("DB-PWI"))
	        {
	            setIp("192.168.24.25");
	            setLogin("");
	            setPass("");
	        }
	        else
	        {
	            setIp("192.168.24.26");
	            setLogin("");
	            setPass("");
	        }

        }

}