import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.iteracja.database.DatabaseNotConnectedException;
import com.iteracja.database.DriverException;
import com.iteracja.database.QueryException;
import com.iteracja.database.QueryResult;

import net.neoremind.sshxcute.core.ConnBean;
import net.neoremind.sshxcute.core.IOptionName;
import net.neoremind.sshxcute.core.SSHExec;
import net.neoremind.sshxcute.exception.TaskExecFailException;
import net.neoremind.sshxcute.task.CustomTask;
import net.neoremind.sshxcute.task.impl.ExecCommand;

public class restarter extends JFrame implements ActionListener
{
        static final long serialVersionUID = 1L;
        JButton restart_p2s, restart_dialer, restart_pause;
        JLabel label_restart_p2s, label_restart_dialer, label_restart_pause,label_restarting, 
                        label_restart_p2s_OK,label_restart_p2s_FAIL,
                        label_restart_dialer_OK,label_restart_dialer_FAIL,
                        label_restart_pause_OK,label_restart_pause_FAIL;
        JComboBox<String> combo;
        
        public restarter()
        {
                setSize(340,320);
                setTitle("p2s restarter");
                setLayout(null);
                setResizable(false);
                
                restart_p2s = new JButton("Restart p2s");
                restart_p2s.setBounds(50, 80, 130, 30);
                add(restart_p2s);
                restart_p2s.addActionListener(this);
                
                restart_dialer = new JButton("Restart dialer");
                restart_dialer.setBounds(50, 130, 130, 30);
                add(restart_dialer);
                restart_dialer.addActionListener(this);
                
                restart_pause = new JButton("Restart pause");
                restart_pause.setBounds(50, 180, 130, 30);
                add(restart_pause);
                restart_pause.addActionListener(this);
                
                label_restart_p2s = new JLabel();
                label_restart_p2s.setBounds(50, 235, 160, 30);
                label_restart_dialer = label_restart_p2s;
                label_restart_pause = label_restart_p2s;
                add(label_restart_p2s);
                add(label_restart_dialer);
                add(label_restart_pause);
                
                label_restart_p2s_OK = new JLabel();
                label_restart_p2s_OK.setBounds(210, 80, 130, 30);
                add(label_restart_p2s_OK);
                
                label_restart_p2s_FAIL = new JLabel();
                label_restart_p2s_FAIL.setBounds(210, 80, 130, 30);
                add(label_restart_p2s_FAIL);
                
                label_restart_dialer_OK = new JLabel();
                label_restart_dialer_OK.setBounds(210, 130, 130, 30);
                add(label_restart_dialer_OK);
                
                label_restart_dialer_FAIL = new JLabel();
                label_restart_dialer_FAIL.setBounds(210, 130, 130, 30);
                add(label_restart_dialer_FAIL);
                
                label_restart_pause_OK = new JLabel();
                label_restart_pause_OK.setBounds(210, 180, 130, 30);
                add(label_restart_pause_OK);
                
                label_restart_pause_FAIL = new JLabel();
                label_restart_pause_FAIL.setBounds(210, 180, 130, 30);
                add(label_restart_pause_FAIL);
                
                label_restarting = new JLabel();
                label_restarting.setBounds(50, 215, 160, 30);
                add(label_restarting);

                combo = new JComboBox<String>();
                combo.setBounds(50, 30, 150, 20);
                
                
                String stm = "SELECT name from data.p2s_restarter_databases";
                try
    			{
    				QueryResult res = Connector.getInstance().executeQuery(stm, true);
    				
                    try
					{
						while(res.next())
						 {
						    System.out.println(res.getString(1));
							combo.addItem(res.getString(1));
						 }
					}
                    catch (QueryException e)
					{
						e.printStackTrace();
					}
    			}
                catch (DatabaseNotConnectedException | DriverException e)
    			{
                	Logger lgr = Logger.getLogger(PreparedStatement.class.getName());
                    lgr.log(Level.SEVERE, e.getMessage(), e);
    				e.printStackTrace();
    			}
                
                add(combo);
                combo.addActionListener(new ComboAction());
        }
        public static void main(String[] args) 
        {

	        restarter r = new restarter();
	        r.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        r.setVisible(true);
                
        }
        @Override
        public void actionPerformed(ActionEvent e) 
        {
                Object source = e.getSource();

                label_restarting.setText(null);
                label_restarting.setText("Restartuje...");
                
                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date date = new Date();
                
                int answer = JOptionPane.showConfirmDialog(null, "Are you sure?", "Are you sure?", JOptionPane.YES_OPTION);
                String ip = ComboAction.getIp();
                String login = ComboAction.getLogin();
                String pass = ComboAction.getPass();
                
                ConnBean cb = new ConnBean(ip, login, pass);
                SSHExec.setOption(IOptionName.SSH_PORT_NUMBER, 9022);
                SSHExec ssh = SSHExec.getInstance(cb);          
                
                CustomTask sampleTask = new ExecCommand("echo 123");
                Font font = new Font("Verdana", Font.BOLD, 17);
                LogIp log = new LogIp();
                
                if(source==restart_p2s)
                {
                        if(answer==JOptionPane.YES_OPTION)
                        {

                                try 
                                {
                                        ssh.connect();
                                        net.neoremind.sshxcute.core.Result res = ssh.exec(sampleTask);
                                        if (res.isSuccess)
                                        {
                                                label_restart_p2s_OK.setFont(font);
                                                label_restart_p2s_OK.setForeground(new Color(34,139,34));
                                                label_restart_p2s_OK.setText("OK");
                                                label_restarting.setText(null);
                                                label_restart_p2s.setText("Restart o: " + dt.format(date));
                                                
                                                log.setAction(1); 
                                        }
                                        else
                                        {
	                                        label_restart_p2s_FAIL.setFont(font);
	                                        label_restart_p2s_FAIL.setForeground(new Color(220,20,60));
	                                        label_restart_p2s_FAIL.setText("FAIL");
	                                        label_restarting.setText(null);
	                                        label_restarting.setText("Blad polaczenia z serwerem");
	                                        
	                                        log.setAction(2);
                                        }
                                }
                                
                                catch (TaskExecFailException e1) 
                                {
				                    System.out.println(e1.getMessage());
				                    e1.printStackTrace();
                                } 
                                catch (Exception e1) 
                                {
				                    System.out.println(e1.getMessage());
				                    e1.printStackTrace();
                                } 
                                finally 
                                {
                            		LogIp.logExecution();
                                    ssh.disconnect();   
                                }
                                
                        }
                }
                else if(source==restart_dialer)
                {
                        if(answer==JOptionPane.YES_OPTION)
                        {

                                try 
                                {
                                        ssh.connect();
                                        net.neoremind.sshxcute.core.Result res = ssh.exec(sampleTask);
                                        if (res.isSuccess)
                                        {
                                                label_restart_dialer_OK.setFont(font);
                                                label_restart_dialer_OK.setForeground(new Color(34,139,34));
                                                label_restart_dialer_OK.setText("OK");
                                                label_restarting.setText(null);
                                                label_restart_p2s.setText("Restart o: " + dt.format(date));
                                                
                                                log.setAction(3); 
					                    }
					                    else
					                    {
                                        label_restart_dialer_FAIL.setFont(font);
                                        label_restart_dialer_FAIL.setForeground(new Color(220,20,60));
                                        label_restart_dialer_FAIL.setText("FAIL");
                                        label_restarting.setText(null);
                                        label_restarting.setText("Blad polaczenia z serwerem");
                                        
                                        log.setAction(4); 
					                    }
                                }
                                
                                catch (TaskExecFailException e1) 
                                {
				                    System.out.println(e1.getMessage());
				                    e1.printStackTrace();
                                } 
                                catch (Exception e1) 
                                {
				                    System.out.println(e1.getMessage());
				                    e1.printStackTrace();
                                } 
                                finally 
                                {
                                    LogIp.logExecution();
                                    ssh.disconnect();       
                                }
                        }
                }
                else if(source==restart_pause)
                {
                        if(answer==JOptionPane.YES_OPTION)
                        {

                                try 
                                {
                                        ssh.connect();
                                        net.neoremind.sshxcute.core.Result res = ssh.exec(sampleTask);
                                        if (res.isSuccess)
                                        {
                                                label_restart_pause_OK.setFont(font);
                                                label_restart_pause_OK.setForeground(new Color(34,139,34));
                                                label_restart_pause_OK.setText("OK");
                                                label_restarting.setText(null);
                                                label_restart_p2s.setText("Restart o: " + dt.format(date));
                                                
                                                log.setAction(5); 
					                    }
					                    else
					                    {
                                        label_restart_pause_FAIL.setFont(font);
                                        label_restart_pause_FAIL.setForeground(new Color(220,20,60));
                                        label_restart_pause_FAIL.setText("FAIL");
                                        label_restarting.setText(null);
                                        label_restarting.setText("Blad polaczenia z serwerem");
                                        
                                        log.setAction(6); 
					                    }
                                }
                                
                                catch (TaskExecFailException e1) 
                                {
				                    System.out.println(e1.getMessage());
				                    e1.printStackTrace();
                                } 
                                catch (Exception e1) 
                                {
				                    System.out.println(e1.getMessage());
				                    e1.printStackTrace();
                                } 
                                finally 
                                {
                                    LogIp.logExecution();
                                    ssh.disconnect();       
                                }
                        }
                }
                else
                {
                        dispose();
                }
        }       
}