import com.iteracja.database.DatabaseAllreadyConnectedException;
import com.iteracja.database.DatabaseConnect;
import com.iteracja.database.DriverException;
import com.iteracja.database.PostgresqlConfig;

public class Connector
{	
	private static DatabaseConnect connect;

	public static DatabaseConnect getInstance()
	{
        return connect;
        
	}
	
	static {
		connect = new DatabaseConnect(new PostgresqlConfig("192.168.16.130", "", "", "pwi2"));
		try
		{
			connect.connect();
			
		} 
		catch (DatabaseAllreadyConnectedException e)
		{
			e.printStackTrace();
		} 
		catch (DriverException e)
		{
			e.printStackTrace();
		}
	}
}
